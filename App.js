// import App from "./src/index";
// export default App;

import corona from "./src/screens/corona";
export default corona;


// import React from 'react';
// import {
//   ScrollView,
//   RefreshControl,
//   StyleSheet,
//   Text,
//   SafeAreaView,
// } from 'react-native';
// import  { Component } from 'react';
//import Constants from 'expo-constants';

// // function wait(timeout) {
// //   return new Promise(resolve => {
// //     setTimeout(resolve, timeout);
// //   });
// // }

// // export default function App() {
// //   const [refreshing, setRefreshing] = React.useState(false);

// //   const onRefresh = React.useCallback(() => {
// //     setRefreshing(true);

// //     wait(5000).then(() => setRefreshing(false));
// //   }, [refreshing]);

// //   return (
// //    // <SafeAreaView style={styles.container}>
// //       <ScrollView
// //      //   contentContainerStyle={styles.scrollView}
// //         refreshControl={
// //           <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
// //         }
// //       >
// //         <Text>Pull down to see RefreshControl indicator</Text>
// //       </ScrollView>
// //   //  </SafeAreaView>
// //   );
// // }

// // const styles = StyleSheet.create({
// //   container: {
// //     flex: 1,
// //     //marginTop: Constants.statusBarHeight,
// //   },
// //   scrollView: {
// //     flex: 1,
// //     backgroundColor: 'pink',
// //     alignItems: 'center',
// //     justifyContent: 'center',
// //   },
// // });





// import React, {Component} from "react";
// import {StyleSheet, View, SafeAreaView} from "react-native";
// //import {Ionicons} from "@expo/vector-icons";
// import shortid from "shortid";
// import {Autocomplete, withKeyboardAwareScrollView} from "react-native-dropdown-autocomplete";

// const data = [
//     "Apples",
//     "Broccoli",
//     "Chicken",
//     "Duck",
//     "Eggs",
//     "Fish",
//     "Granola",
//     "Hash Browns",
//   ];
// class HomeScreen extends Component {
    
//   handleSelectItem(item, index) {
//     const {onDropdownClose} = this.props;
//     onDropdownClose();
//     console.log(item);
//   }

//   render() {
//     const autocompletes = [...Array(1).keys()];
  
//     const apiUrl = "https://5b927fd14c818e001456e967.mockapi.io/branches";

//     const {scrollToInput, onDropdownClose, onDropdownShow} = this.props;

//     return (
//       <View style={styles.autocompletesContainer}>
//         <SafeAreaView>
//             <Autocomplete
//             //  key={shortid.generate()}
//               style={styles.input}
//             //   scrollToInput={ev => scrollToInput(ev)}
//             //   handleSelectItem={(item, id) => this.handleSelectItem(item, id)}
//             //   onDropdownClose={() => onDropdownClose()}
//             //   onDropdownShow={() => onDropdownShow()}
//             //   renderIcon={() => (
//             //     <Ionicons name="ios-add-circle-outline" size={20} color="#c7c6c1" style={styles.plus} />
//             //   )}
//               //fetchDataUrl={apiUrl}
//               data={data}
//             //   valueExtractor={item => item}
//             //   minimumCharactersCount={2}
//             //   highlightText
//             //   valueExtractor={item => item.name}
//             //   rightContent
//           //    rightTextExtractor={item => item.properties}
//             />
          
//         </SafeAreaView>
//       </View>
//     );
//   }
// }
// const styles = StyleSheet.create({
//   autocompletesContainer: {
//     paddingTop: 0,
//     zIndex: 1,
//     width: "100%",
//     paddingHorizontal: 8,
//   },
//   input: {maxHeight: 40},
//   inputContainer: {
//     display: "flex",
//     flexShrink: 0,
//     flexGrow: 0,
//     flexDirection: "row",
//     flexWrap: "wrap",
//     alignItems: "center",
//     borderBottomWidth: 1,
//     borderColor: "#c7c6c1",
//     paddingVertical: 13,
//     paddingLeft: 12,
//     paddingRight: "5%",
//     width: "100%",
//     justifyContent: "flex-start",
//   },
//   container: {
//     flex: 1,
//     backgroundColor: "#ffffff",
//   },
//   plus: {
//     position: "absolute",
//     left: 15,
//     top: 10,
//   },
// });

// export default withKeyboardAwareScrollView(HomeScreen);


/*This is an example of AutoComplete Input/ AutoSuggestion Input*/
// import React, { Component } from 'react';
// //import react in our code.
// import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
// //import all the components we are going to use.
// import Autocomplete from 'react-native-autocomplete-input';
// //import Autocomplete component
 
// const API = 'https://swapi.co/api';
// //Demo base API to get the data for the Autocomplete suggestion
// class App extends Component {
//   constructor(props) {
//     super(props);
//     //Initialization of state
//     //films will contain the array of suggestion
//     //query will have the input from the autocomplete input
//     this.state = {
//       //films: [{"name":"reda"},{"name":"mreda"}],
//       films: ["reda","mreda","shabaan","امكسسلين"],
//       query: '',
//     };
//   }
//   componentDidMount() {
//     //First method to be called after components mount
//     //fetch the data from the server for the suggestion
//     fetch(`${API}/films/`)
//       .then(res => res.json())
//       .then(json => {
//         const { results: films } = json;
//        // this.setState({ films });
//         //setting the data in the films state
//         console.log("list"+this.state.films[0]);
//       });
//   }
//   findFilm(query) {
//     //method called everytime when we change the value of the input
//     if (query === '') {
//       //if the query is null then return blank
//       return [];
//     }
 
//     const { films } = this.state;
//     //making a case insensitive regular expression to get similar value from the film json
//     const regex = new RegExp(`${query.trim()}`, 'i');
//     //return the filtered film array according the query from the input
//     console.log("search: "+films.filter(film => film.search(regex) >= 0))
//     return films.filter(film => film.search(regex) >= 0);
//   }
 
//   render() {
//     const { query } = this.state;
//     const films = this.findFilm(query);
//     const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
 
//     return (
//       <View style={styles.container}>
//         <Autocomplete
//           autoCapitalize="none"
//           autoCorrect={false}
//           containerStyle={styles.autocompleteContainer}
//           //data to show in suggestion
//           data={films.length === 1 && comp(query, films[0]) ? [] : films}
//           //default value if you want to set something in input
//           defaultValue={query}
//           /*onchange of the text changing the state of the query which will trigger
//           the findFilm method to show the suggestions*/
//           onChangeText={text => this.setState({ query: text })}
//           placeholder="Enter the film title"
          
//           renderItem={({ item }) => (
//             //you can change the view you want to show in suggestion from here
//             <TouchableOpacity onPress={() => this.setState({ query: item})}>
//               <Text style={styles.itemText}>
//                 {item}
//               </Text>
//             </TouchableOpacity>
//           )}
//         />
//       </View>
//     );
//   }
// }
// const styles = StyleSheet.create({
//   container: {
//     backgroundColor: 'red',
//     flex: 1,
//     padding: 16,
//     marginTop: 40,
//   },
//   autocompleteContainer: {
//    // backgroundColor: 'green',
//     borderWidth: 0,
//     width:300,
//     maxHeight:100,
//   },
//   descriptionContainer: {
//     flex: 1,
//     justifyContent: 'center',
//   },
//   itemText: {
//     fontSize: 30,
//     paddingTop: 5,
//     paddingBottom: 5,
//     margin: 2,
//   },
 
// });
// export default App;

// import React, { Component } from 'react';
// import { Container, Header ,Text} from 'native-base';
// import { Col, Row, Grid } from 'react-native-easy-grid';
// export default class LayoutExample extends Component {
//   render() {
//     return (
//       <Container>
//         <Header />
//         <Grid>
//           <Col style={{ backgroundColor: '#635DB7',  height: 200 ,margin:10 ,marginTop:100 ,borderRadius:30}}> 
//           <Text style={{alignContent:'center'}}>10</Text></Col>
//           <Col style={{ backgroundColor: '#00CE9F', height: 200 ,margin:10,marginTop:100,borderRadius:30}}></Col>
//         </Grid>
//         <Grid>
//           <Col style={{ backgroundColor: '#635DB7', height: 200 ,margin:10 ,borderRadius:30}}></Col>
//           <Col style={{ backgroundColor: '#00CE9F', height: 200 ,margin:10 ,borderRadius:30}}></Col>
//         </Grid>
//       </Container>
//     );
//   }
// }