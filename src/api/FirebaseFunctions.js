import firebase from '../firebase';

class FirebaseFunctions {

  constructor() {
    this.currentUser = null;
    this.profileInfo = {};
    this.userEvents = {};
    this.userEvents = [];
    this.allData = [];
    this.userExpireEvents = [];
    this.userEvenetsIds = [];
    this.currentDate = Date.now() / 1000;
    this.firebaseConnection = firebase;
  }

  getCurrentUser() {
    if (this.currentUser) {
      return this.currentUser;
    } else {
      this.currentUser = this.firebaseConnection.auth().currentUser;
      return this.currentUser;
    }
  }

  async getProfileInfo() {

    this.currentUser = this.getCurrentUser();
    if (this.currentUser) {
      // const logedin = true;
      const result = await this.firebaseConnection
        .database()
        .ref('users/' + this.currentUser.uid).once('value').catch((error) => alert(error.message));
      const resultValues = result.val();
      this.profileInfo = {
        firstName: resultValues.firstName ? resultValues.firstName : '',
        lastName: resultValues.lastName ? resultValues.lastName : '',
        mobileNumber: resultValues.mobileNumber ? resultValues.mobileNumber : '',
        address: resultValues.address ? resultValues.address : '',
        gender: resultValues.gender ? resultValues.gender : '',
        age: resultValues.age ? resultValues.age : '',
      };
      // Save user events Ids
      this.userEvenetsIds = Object.values(resultValues.events);

      return this.profileInfo;
    }
  }

  async editProfileInfo(userId, profileInfo) {
    if (userId) {
      try {
        await firebase.database().ref('users/' + userId).update(profileInfo);
        return true;
      } catch (error) {
        return error.message;
      }
    }
  }

  async getUserEvents() {

    if (this.userEvenetsIds) {

      let userEvents = [];
      let userExpireEvents = [];
      for (const eventId of this.userEvenetsIds) {
        await this.firebaseConnection.database().ref('events/' + eventId).once('value', (snapshot) => {
          const event = snapshot.val();
          if (event && event.expireDate > this.currentDate) {
            event.eventId = eventId;
            userEvents.push(event);
          } else if (event) {
            event.eventId = eventId;
            userExpireEvents.push(event);
          }
        }).catch((error) => alert(error));
      }
      this.userEvents = userEvents;
      this.userExpireEvents = userExpireEvents;
      return { userEvents: this.userEvents, userExpireEvents: this.userExpireEvents };
    }
  }

  async deleteUserEvent(eventId) {
    try {
      await this.firebaseConnection.database().ref('events/').child(eventId).remove();
      await this.firebaseConnection.database().ref('users/').child(currentUser.uid).child('events').once('value').then((snapshot) => {
        snapshot.forEach(async (childSnapshot) => {
          if (childSnapshot.val() == eventId) {
            await this.firebaseConnection.database().ref('users/').child(currentUser.uid).child('events').child(childSnapshot.key).remove();
          }
        });
      }).catch((error => alert(error)));

      // Get user events
      const userEvents = this.getUserEvents();
      return userEvents;

    } catch (e) {
      alert(e);
    }
  }

  async editUserEvent(eventId, eventData) {
    try {
      await firebase
        .database()
        .ref('events/' + eventId)
        .update(eventData);
      // Get user events
      const userEvents = await this.getUserEvents();
      return userEvents;

    } catch (e) {
      alert(e);
    }
  }

  async signOut() {
    try {
      await firebase.auth().signOut();
      return true;
    } catch (error) {
      return error.message;
    }
  }




  //home functionality
  async getALLMedicin() {
    let allData = [];
    let validData= [];
    try {
      await firebase
      .database()
      .ref('/events')
      .orderByKey()
      .once('value')
      .then(snapshot => {
      // Get user events
      const events = snapshot.val();
      allData = Object.values(events);
      if(events!=null){
      this.allData=allData;
      }
    })

    } catch (e) {
      alert(e);
    }
    for (const event of allData) {
      if (event && event.expireDate > this.currentDate) {
        validData.push(event);
      }
    }
    return validData;
  }
   //check user if login 
   async checkUserLogin(){
      let usertemp ;
      await firebase.auth().onAuthStateChanged(user => {
      if (user) {
        
        usertemp=user;
      } 
      })
      return usertemp;
}

//search in main
async mainSearch(value){
  let allData = [];
  let validData= [];
  console.log("in main search"+value);
  if(value==null || value==''){
    console.log("in main search in if "+value);
    await firebase
    .database()
    .ref('/events')
    .orderByKey()
    .once('value')
    .then(snapshot => {
      const events = snapshot.val();
      if(events!=null){
      allData = Object.values(events);
      }
    })
    .catch(error => alert(error.message));
  }else{
    console.log("in main search out if  "+value);
    await firebase
    .database()
    .ref('/events')
    .orderByChild('name')
    .equalTo(value)
    .once('value')
    .then(snapshot => {
      const events = snapshot.val();
      console.log("date"+events);
      if(events!=null){
       allData = Object.values(events);
      }
    })
    .catch(error => alert(error.message+"reda"+value));
  }
  for (const event of allData) {
    console.log("date"+event.expireDate+ "   "+this.currentDate)
    if (event && event.expireDate > this.currentDate) {
      console.log("dateaaa"+event.expireDate+ "   "+this.currentDate)
      validData.push(event);
    }
  }
  return validData;
}

//add new medicin
async addNewMedicin(medicinInfo){
  this.currentUser = this.getCurrentUser();
  console.log("userID"+this.currentUser.uid);
  const eventKey = 
  await this.firebaseConnection
      .database()
      .ref('/events')
      .push(medicinInfo)
      .getKey();

  if (eventKey) {
      await this.firebaseConnection
          .database()
          .ref('users/')
          .child(this.currentUser.uid)
          .child('/events/')
          .push(eventKey);
  }
  return

}
}

export default new FirebaseFunctions();

