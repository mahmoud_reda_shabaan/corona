import * as firebase from 'firebase';

// Your web app's Firebase configuration
const config = {
  apiKey: "AIzaSyC0aQhIo53Ep2nK7bvnvWS5gPM5L3k3rXw",
  authDomain: "matches-29aa2.firebaseapp.com",
  databaseURL: "https://matches-29aa2.firebaseio.com",
  projectId: "matches-29aa2",
  storageBucket: "matches-29aa2.appspot.com",
  messagingSenderId: "139486011717",
  appId: "1:139486011717:web:5dfd155bc5e3c1a3055e2c",
  measurementId: "G-TRQZB8PTH5"
};



  firebase.initializeApp(config);

  export default firebase;