import React, {Component} from 'react';
import { createRootNavigator,SignedIn,SignedOut, } from "./router";
import { isSignedIn } from "./auth";
import {View,Text} from 'react-native';
import Home from "./screens/Home";


export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      signedIn: false,
      checkedSignIn: false
    };
  }

  componentDidMount() {
    isSignedIn()
      .then(res => this.setState({ signedIn: res, checkedSignIn: true }))
      .catch(err => alert("An error occurred"));
  }

  render() {
    const { checkedSignIn, signedIn } = this.state;

    // If we haven't checked AsyncStorage yet, don't render anything (better ways to do this)
    // if (!checkedSignIn) {
    //   return null;
    // }

    const Layout = createRootNavigator(false);
    //alert("after : " +Layout);
    // return <SignedIn />;//<Layout/>;
    return <Layout/>;

 // return <Home />;
  }
}
