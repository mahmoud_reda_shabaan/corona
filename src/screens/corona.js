import React, {Component} from 'react';
import {
  Image,
  TouchableOpacity,
  View,
  StyleSheet,
  ScrollView,
  RefreshControl,
  SafeAreaView,
  Dimensions,
  ImageBackground,
  Platform,
  YellowBox,
} from 'react-native';
import firebase from '../firebase';
import {
  Table,
  TableWrapper,
  Row,
  Rows,
  Col,
  Cols,
  Cell,
} from 'react-native-table-component';
import {Text, Card, Divider} from 'react-native-elements';
import {
  Button,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Title,
  Container,
  Content,
  CardItem,
  Thumbnail,
  ListItem,
  Fab,
  CheckBox,
  Picker,
  Form,
  Item,
  Input,
  DatePicker,
  Label,
} from 'native-base';
import {
  AdMobBanner,
  AdMobInterstitial,
  AdMobRewarded,
  PublisherBanner,
} from 'react-native-admob';

const BannerExample = ({style, title, children, ...props}) => (
  <View {...props} style={[styles.example, style]}>
    <Text style={styles.title}>{title}</Text>
    <View>{children}</View>
  </View>
);

const { width, height } = Dimensions.get('window');
console.disableYellowBox = true; 
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.navigation = props.navigation;
    this.firebaseConnection = firebase;
    // this.callbackAddEvent = this.callbackAddEvent.bind(this);
    this.state = {
      status: '',
      error: '',
      refreshing: false,
      image: '',
      searchvalue: '',
      currentUser: null,
      events: [],
      value: '',
      addEvent: false,
      SearchClick: false,
      allData: [],
      alljson: {},
      films: [
        'reda',
        'mreda',
        'shabaan',
        'امكسسلين',
        'mreda',
        'shabaan',
        'امكسسلين',
      ],
      query: '',
      tableHead: ['Country', 'CONFIRMED CASES', 'REPORTEDDEATHS'],
      tableData: [[]],
      confirmedCase: '',
      reporteddeaths: '',
      currentDate: '',
      // [
      //   ['Chine', '90000', '300'],
      //   ['Iran', '100', '52'],
      //   ['Egypt', '2', '0'],
      //   ['japan', '3', '0']
      // ]
    };
  }
  async loadAllData() {
    this.setState({tableData: []});
    this.setState({confirmedCase: ''});
    this.setState({reporteddeaths: ''});
    this.setState({currentDate: ''});

    fetch('https://infographics.channelnewsasia.com/covid-19/newdata.json')
      .then(response => response.json())
      .then(data => {
        console.log(data);
        const alljson = data;
        this.setState({alljson: alljson});
        this.setState({allData: alljson.feed.entry});
        // console.log(this.state.alljson.version);
        // console.log(this.state.allData[0].title.$t);
        console.log('count : ' + this.state.allData.length);
        const tableData = [[]];
        var confirmedCases = 0;
        var deathCases = 0;
        var date;
        date = this.state.alljson.feed.updated.$t;
        for (let i = 0; i < this.state.allData.length; i++) {
          console.log(i);

          const dataobj = [];

          dataobj[0] = this.state.allData[i].gsx$country.$t;
          dataobj[1] = this.state.allData[i].gsx$confirmedcases.$t
            .toString()
            .replace(',', '');
          var text = this.state.allData[i].gsx$reporteddeaths.$t;
          var text2 = text.toString().replace(',', '');
          //console.log(this.state.allData[i].gsx$country.$t +" : "+text2);
          if (text2 != '') {
            dataobj[2] = text2;
            // console.log(text2);
          } else {
            dataobj[2] = '0';
          }
          //confirmedCases =parseInt(this.state.allData[12].gsx$reporteddeaths.$t);

          tableData[i] = dataobj;
        }

        tableData.sort((a, b) => b['1'] - a['1']);
        this.setState({tableData: tableData});
        for (let j = 0; j < this.state.tableData.length; j++) {
          confirmedCases =
            confirmedCases + parseInt(this.state.tableData[j][1]);
          deathCases = deathCases + parseInt(this.state.tableData[j][2]);
        }
       // console.log(confirmedCases);
        confirmedCases = this.numberWithCommas(confirmedCases);
       // console.log(confirmedCases);
       // console.log(deathCases);
        deathCases = this.numberWithCommas(deathCases);
        //console.log(date.substring(0, 19));
        this.setState({confirmedCase: confirmedCases});
        this.setState({reporteddeaths: deathCases});
        this.setState({
          currentDate: date.substring(0, 10) + ' ' + date.substring(11, 19),
        });
      })
      .catch //error => console.log(error)
      ();
  }
  async componentDidMount() {
    this.getErrorData();
    this.loadAllData();

    // AdMob start
    AdMobRewarded.setTestDevices([AdMobRewarded.simulatorId]);
    AdMobRewarded.setAdUnitID('ca-app-pub-3988969113543344/5100987861');

    AdMobRewarded.addEventListener('rewarded', reward =>
      console.log('AdMobRewarded => rewarded', reward),
    );
    AdMobRewarded.addEventListener('adLoaded', () =>
      console.log('AdMobRewarded => adLoaded'),
    );
    AdMobRewarded.addEventListener('adFailedToLoad', error =>
      console.warn(error),
    );
    AdMobRewarded.addEventListener('adOpened', () =>
      console.log('AdMobRewarded => adOpened'),
    );
    AdMobRewarded.addEventListener('videoStarted', () =>
      console.log('AdMobRewarded => videoStarted'),
    );
    AdMobRewarded.addEventListener('adClosed', () => {
      console.log('AdMobRewarded => adClosed');
      AdMobRewarded.requestAd().catch(error => console.warn(error));
    });
    AdMobRewarded.addEventListener('adLeftApplication', () =>
      console.log('AdMobRewarded => adLeftApplication'),
    );

    AdMobRewarded.requestAd().catch(error => console.warn(error));

    AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
    AdMobInterstitial.setAdUnitID('ca-app-pub-3988969113543344/5100987861');

    AdMobInterstitial.addEventListener('adLoaded', () =>
      console.log('AdMobInterstitial adLoaded'),
    );
    AdMobInterstitial.addEventListener('adFailedToLoad', error =>
      console.warn(error),
    );
    AdMobInterstitial.addEventListener('adOpened', () =>
      console.log('AdMobInterstitial => adOpened'),
    );
    AdMobInterstitial.addEventListener('adClosed', () => {
      console.log('AdMobInterstitial => adClosed');
      AdMobInterstitial.requestAd().catch(error => console.warn(error));
    });
    AdMobInterstitial.addEventListener('adLeftApplication', () =>
      console.log('AdMobInterstitial => adLeftApplication'),
    );

    AdMobInterstitial.requestAd().catch(error => console.warn(error));
    //AdMob end
  }
  componentWillUnmount() {
    AdMobRewarded.removeAllListeners();
    AdMobInterstitial.removeAllListeners();
  }

  showRewarded() {
    AdMobRewarded.showAd().catch(error => console.warn(error));
  }

  showInterstitial() {
    AdMobInterstitial.showAd().catch(error => console.warn(error));
  }

  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  async onRefresh() {
    //console.log('redaaaa     :');
    this.getErrorData();
    //const data = this.state.allData;
    this.setState({refreshing: true});
    // const allData= await FirebaseFunctions.getALLMedicin();
    this.loadAllData();
    // this.setState({ allData: data });
    this.setState({refreshing: false});

    //this.fetchData();
  }
  async getErrorData() {
    try {
      const result = await this.firebaseConnection
        .database()
        .ref('/corona')
        .once('value')
        .catch(error => alert(error.message));
      const caseDate = result.val();
      //console.log('res' + caseDate.status);
      this.setState({status: caseDate.status.toString()});
      this.setState({error: caseDate.error.toString()});
      //console.log('res' + this.state.status);
    } catch (e) {
      alert(e);
    }
  }

  render() {
    const {query, allData} = this.state;
    // const films = this.findFilm(query);
    // const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
    return (
      <Container Style={styles.container}>
        <Header
          style={{backgroundColor: '#576180'}}
          // searchBar
          rounded
          androidStatusBarColor="#576180">
          <Left>
            <Button
              transparent
              //  onPress={() => this.props.navigation.openDrawer()}
            ></Button>
          </Left>

          <Body
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center',
              paddingLeft: 20,
            }}>
            <Text style={{color: 'white'}}>Corona Monitor</Text>
          </Body>
          <Right style={{marginRight: 25}}>
            <Button transparent onPress={this.onRefresh.bind(this)}>
              <Icon style={{color: '#29ff07', fontSize: 35}} name="refresh" />
            </Button>
          </Right>
        </Header>
        {/* <ImageBackground source={require('../images/coronavirus.jpg')} style={{width: '100%', height: '100%'}}> */}
        {/* refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.onRefresh.bind(this)}
          />} */}
        <Content>
          {this.state.status == 'false' && (
            <Card containerStyle={styles.cardError}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Text style={styles.textError}>{this.state.error}</Text>
              </View>
            </Card>
          )}

          <Card containerStyle={styles.card}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text style={styles.notes}>confirmed cases :</Text>
              <Text style={styles.price}>{this.state.confirmedCase}</Text>
            </View>
            <Divider style={{backgroundColor: '#000000', marginVertical: 20}} />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text style={styles.notes}>reported deaths :</Text>
              <Text style={styles.price}>{this.state.reporteddeaths}</Text>
            </View>
            <Divider style={{backgroundColor: '#000000', marginVertical: 20}} />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <Text style={styles.notes}>last update:</Text>
              <Text style={styles.price}>{this.state.currentDate}</Text>
            </View>
          </Card>
          

          <View style={{marginLeft: 20, marginRight: 20}}>
          <BannerExample>
            <AdMobBanner
              adSize="banner"
              adUnitID="ca-app-pub-3988969113543344/2042815346"
              testDevices={[AdMobBanner.simulatorId]}
              onAdFailedToLoad={error => console.log(error)}
              ref={el => (this._basicExample = el)}
            />
            <Button
              transparent
              title="Reload"
              onPress={() => this._basicExample.loadBanner()}
            />
          </BannerExample>
            <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
              <Row
                data={this.state.tableHead}
                style={styles.head}
                textStyle={styles.texthed}
              />
            </Table>
            <ScrollView
              style={{
                height: 300,
                borderBottomColor: '#f1f8ff',
                borderBottomWidth: 2,
              }}
              nestedScrollEnabled={true}>
              <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                <Rows data={this.state.tableData} textStyle={styles.text} />
              </Table>
            </ScrollView>
            <BannerExample>
              <AdMobBanner
                adSize="banner"
                adUnitID="ca-app-pub-3988969113543344/5100987861"
                testDevices={[AdMobBanner.simulatorId]}
                onAdFailedToLoad={error => console.log(error)}
                ref={el => (this._basicExample = el)}
              />
              <Button
                transparent
                title="Reload"
                onPress={() => this._basicExample.loadBanner()}
              />
            </BannerExample>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#c8e1ff',
    //'rgba(56, 172, 236, 1)',
    borderWidth: 0,
    borderRadius: 20,
    marginBottom: 0,
  },
  cardError: {
    backgroundColor: '#B2F019',
    //'rgba(56, 172, 236, 1)',
    borderWidth: 0,
    borderRadius: 20,
    marginBottom: 10,
  },
  textError: {
    fontSize: 18,
    color: '#FD0C00',
    textTransform: 'capitalize',
  },
  time: {
    fontSize: 38,
    color: '#fa878d',
  },
  notes: {
    fontSize: 18,
    color: '#000000',
    textTransform: 'capitalize',
  },
  price: {
    color: '#02a134',
    fontSize: 18,
    textTransform: 'capitalize',
  },
  header: {
    fontSize: 25,
    color: '#fff',
  },

  container: {
    flex: 1,
    padding: 16,
    paddingTop: 30,
    backgroundColor: '#fff',
    margin: 20,
    marginTop: Platform.OS === 'ios' ? 30 : 10,
  },
  head: {height: 40, backgroundColor: '#f1f8ff'},
  text: {margin: 6, color: '#87264A', fontSize: 15},
  texthed: {margin: 6},
  example: {
    paddingVertical: 2,
    //backgroundColor: 'red',
    //width:350,
    alignItems:'center',
  },
  title: {
    //margin: 10,
    fontSize: 20,
  },
});
